# MecanumCartPole_ROS

#### 介绍
基于麦克纳姆轮全向平台和ROS的平面二自由度倒立摆

#### 软件架构
* 1.jetcam:驱动双摄像头并发布话题
* 2.opencv_cart_pole:视觉测角、发布控制命令
* 3.robot_pose_ekf:ROS官方包，用于对位姿进行滤波
* 4.turn_on_wheeltec_robot:机器人底层通信、控制
* 5.wheeltec_robot_urdf:存储机器人模型，用于可视化


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  roslaunch turn_on_wheeltec_robot turn_on_wheeltec_robot.launch
2.  rosrun jetcam jetcam_test.py
3.  rosrun opencv_cart_pole opencv_cart_pole.py

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
